# from django.shortcuts import render
# from django.http import HttpResponse

from django.http import JsonResponse
from web3 import Web3

import json 

def index(request):
    w3 = Web3(Web3.HTTPProvider('https://mainnet.infura.io/v3/35cb5c72db0c4f348f8784760b7efb92'))
    block = w3.eth.get_block('latest')
    data = list(block.items())
    # response = JsonResponse(json.dumps(block), safe=False)

    return JsonResponse("Block. %s" % data, safe=False)
