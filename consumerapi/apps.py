from django.apps import AppConfig


class ConsumerapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'consumerapi'
